var express = require('express');
var xml = require('xml');
var bodyParser = require('body-parser')
var fs = require('fs');
//var server = express.createServer();
// express.createServer()  is deprecated. 
var server = express(); // better instead
server.use( bodyParser.json() );
server.use('/css', express.static(__dirname + '/css'));
server.use('/js', express.static(__dirname + '/js'));
server.use('/lib', express.static(__dirname + '/lib'));
server.use(express.static(__dirname + '/'));

server.post('/render/svg',function(req,res){
	const TextToSVG = require('text-to-svg');
	const textToSVG = TextToSVG.loadSync( 'fonts/' + req.body.font +'.ttf');
	// { text:'Online Text to SVG Generator', fill: 'dimgrey',stroke:'black',font:'IndieFlower-Regular', size:'72', anchor:'top'}
	 
	const attributes = {fill: req.body.fill||'dimgrey', stroke: req.body.stroke||'black'};
	const options = {x: 0, y: 0, fontSize: req.body.size||72, anchor: req.body.anchor||'top', attributes: attributes};
	 console.log(req.body);
	const svg = textToSVG.getSVG(req.body.text || 'empty', options);
 
	res.setHeader('Content-Type', 'text/xml');  
	res.setHeader('Content-Length', svg.length);
	res.setHeader('Content-disposition', 'attachment; filename=generate.svg');
	res.send(svg);
})

server.listen(8000);